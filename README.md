# API | Sistema Restaurante

Este proyecto contiene toda la parte de backend del sistema.

**Nombre del proyecto:** Sistema Restaurante Don Birrachón

## Integrantes

El equipo es conformado por las siguientes personas:

- Fabián Fernández Solano.
- Manfred Taylor Dejuk.
- Fabián Sanchez Muñoz.
- Sebastian Flores Escalante.

## Descripción del proyecto

El proyecto consiste en crear un sistema que maneje distintas areas de un restaurante, asi como proveer a este de un sistema de entrega y pedidos en línea.

## Instalación

A continuación, se presentan los pasos para tener un ambiente local listo para desarrollo.

### Paso 1

Una vez clonado el repositorio, es necesario agregar un archivo llamado `.env` en la raíz del proyecto con los siguientes valores:

- DB_HOST: Host de base de datos.
- DB_NAME: Nombre de base de datos.
- DB_USER: Usuario de base de datos.
- DB_PASS: Contraseña de base de datos.
- DB_PORT: Puerto usado para conectar con la base de datos..
- API_PORT: Puerto por el que el backend va a escuchar peticiones.

_Nota: Para las credenciales de cada ambiente (DEV, PRE y PROD), preguntar a un miembro del equipo._

### Paso 2

Para ejecutar el proyecto (una vez completado el paso 1), solo se deben ejecutar los siguientes comandos en la terminal o cmd:

```bash
    # Instalar dependencias
    npm install

    # Ejecutar en ambiente de DESARROLLO
    npm run dev

    # Ejecutar en ambiente de PRODUCCION
    npm run prod
```
