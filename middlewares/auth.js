const _ = require('lodash');
const jwt = require('jsonwebtoken');
const fs = require('fs');

const audience =
  'RestauranteDonBirrachon-435frdver435rearr43-4354freg325342-dfewf43tf4t343t';

exports.genToken = (user, pass) => {
  return jwt.sign({ user, pass }, audience);
};

exports.verifyToken = (token) => {
  return jwt.verify(token, audience, (err, decoded) => {
    if (err) throw new Error('Invalid Token: ' + err);
    console.log(decoded);
  });
};

exports.authMiddleware = async (req, res, next) => {
  const authHeader = req.header('Authorization');
  if (!authHeader) return res.status(401).send('Not authorized');

  const token = _.replace(authHeader, 'Bearer', '').trim();
  if (!token) return res.status(401).send('Not authorized');

  try {
    console.log('Decoding...');
    const decode = jwt.verify(token, audience, (err, decoded) => {
      if (err) throw new Error('Invalid Token: ' + err);
      console.log(decoded);
    });
    req.user = decode;
    next();
  } catch (error) {
    return res.status(401).send('Not authorized');
  }
};
